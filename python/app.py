import json
from flask import Flask, request
from lstm import predict_next_value, prepare_model

app = Flask(__name__)

@app.route('/nextValues', methods = ['POST'])
def next_value():
  if request.method == 'POST':
    json_values = request.json['values']
    values = [float(x) for x in json_values]
    n_predictions = int(request.json['n_predictions'])

    model = prepare_model(values)
    results = []

    for i in range(n_predictions):
      next_value = predict_next_value(model, values)
      values.append(next_value[0][0])
      results.append(next_value[0][0])

    results = str(results)

    return results

if __name__ == '__main__':
  app.run()