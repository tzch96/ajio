import numpy as np
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense
import time
 
"""
Split the input sequence into samples.
We are operating on a univariate sequence (for example a time series)
to predict the next value based on the previous values.

For a sequence e.g. [1, 2, 3, 4, 5, 6, 7, 8, 9]
we take "n_steps"-sized samples "x" e.g. [1, 2, 3], [2, 3, 4], and so on,
where the output "y" is respectively 4, 5, and so on.

The LSTM model will attempt to learn from examples of such sequences of observations.
"""
def split_data(input_data, n_steps = 30):
	X, y = [], []
	
	for i in range(len(input_data)):
		last = i + n_steps
	
		# exit the loop if we are at the end of the pattern
		if last + 1 > len(input_data):
			break
	
		# make the sequences and add them to the input-output pairs
		X.append(input_data[i : last])
		y.append(input_data[last])
	
	return np.array(X), np.array(y)

# TODO maybe choose better/faster activation, optimizer, loss functions
def prepare_model(input_data, n_steps = 30, n_features = 1, activation = 'relu', optimizer = 'adam', loss = 'mse', epochs = 500):
	# prepare the data first
	# split the input data into samples
	X, y = split_data(input_data, n_steps)
	# reshape data to fit Keras sequential model input shape
	X = X.reshape((X.shape[0], X.shape[1], n_features))

	# the model itself
	# a simple small LSTM network with a single hidden LSTM layer and a single output layer
	model = Sequential()
	model.add(LSTM(50, activation = activation, input_shape = (n_steps, n_features)))
	# stacked LSTM
	# model.add(LSTM(50, activation = activation, return_sequences = True, input_shape = (n_steps, n_features)))
	# model.add(LSTM(50, activation = activation))
	model.add(Dense(1))
	model.compile(optimizer = optimizer, loss = loss)
	# fit the model with previously prepared data
	model.fit(X, y, epochs = epochs, verbose = 2)

	return model

def prepare_input(input_data, n_steps = 30, n_features = 1):
	input_data = np.array(input_data[-n_steps : ])
	input_data = input_data.reshape((1, n_steps, n_features))
	return input_data

def predict_next_value(model, input_data, n_steps = 30, n_features = 1):
	x_input = prepare_input(input_data, n_steps, n_features)

	y = model.predict(x_input, verbose = 0)

	return y

def test(placeholder_input, n_predictions):
	input_size = len(placeholder_input)

	model = prepare_model(placeholder_input)

	# get next n_predictions values
	for i in range(n_predictions):
		next_value = predict_next_value(model, placeholder_input)
		placeholder_input.append(next_value[0][0])

	print(placeholder_input[input_size : ])

# simple task of counting next numbers
# placeholder_input_0 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
# 											11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
# 											21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
# 											31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
# 											41, 42, 43, 44, 45, 46, 47, 48, 49, 50]

# test(placeholder_input_0, 50)

# placeholder input sequence - some SP500 values from December 2019 (next is 323.739990)
# the chosen data is specific in that we can see a pretty constant short-term growth trend,
# so we expect the same in the output (real-life datasets will be much larger with much more variation)
# placeholder_input_1 = [313.820007, 314.029999, 314.429993, 316.869995, 319.220001, 319.920013,
# 	320.000000, 319.799988, 320.459991, 321.589996, 321.470001, 321.649994]

# test(placeholder_input_1, 1)