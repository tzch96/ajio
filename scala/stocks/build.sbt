ThisBuild / scalaVersion     := "2.13.3"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "pl.tzch96"
ThisBuild / organizationName := "tzch96"

val localDeps = Seq(
  evolutions,
  guice,
  jdbc,
  "org.postgresql"             %  "postgresql"         % "42.2.18",
  "org.playframework.anorm"    %% "anorm"              % "2.6.8",
  "org.scalatestplus.play"     %% "scalatestplus-play" % "5.0.0" % Test,
  "ch.qos.logback"             %  "logback-classic"    % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging"      % "3.9.2",
  "org.jsoup"                  %  "jsoup"              % "1.13.1",
  "org.apache.httpcomponents"  %  "httpclient"         % "4.5.13"
)

lazy val root = (project in file(".")).
  settings(
    name := "stocks",
    libraryDependencies ++= localDeps
  ).enablePlugins(PlayScala)