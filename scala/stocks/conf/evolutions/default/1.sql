-- Create basic database schema
-- !Ups
CREATE TABLE IF NOT EXISTS stock_prices (
  price_id serial PRIMARY KEY,
  ticker VARCHAR(10) NOT NULL,
  price_date DATE NOT NULL,
  close_price NUMERIC NOT NULL
);

CREATE TABLE IF NOT EXISTS user_stocks (
  user_stock_id serial PRIMARY KEY,
  ticker VARCHAR(10) NOT NULL,
  buy_date DATE NOT NULL,
  sell_date DATE,
  current_value NUMERIC NOT NULL
);

CREATE SEQUENCE IF NOT EXISTS s_stock_prices INCREMENT 1 START 100;
CREATE SEQUENCE IF NOT EXISTS s_user_stocks INCREMENT 1 START 100;

-- TODO update current_value to current value automatically

-- !Downs
COMMENT ON TABLE play_evolutions IS 'Down 1';

-- DROP TABLE stock_prices;
-- DROP TABLE user_stocks;