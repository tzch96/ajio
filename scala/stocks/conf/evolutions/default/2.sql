-- !Ups
ALTER TABLE user_stocks ADD COLUMN shares INTEGER;

CREATE UNIQUE INDEX stock_prices_td_idx ON stock_prices(ticker, price_date);
CREATE UNIQUE INDEX user_stocks_td_idx ON user_stocks(ticker, buy_date);

-- !Downs
COMMENT ON TABLE play_evolutions IS 'Down 2';