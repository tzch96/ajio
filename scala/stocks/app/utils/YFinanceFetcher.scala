package utils

import com.typesafe.scalalogging.Logger
import javax.inject._
import play.api.Configuration

import scala.language.postfixOps
import sys.process._
import java.io.File
import java.net.{URL, HttpURLConnection}
import java.text.SimpleDateFormat
import java.util.Date

@Singleton
class YFinanceFetcher @Inject()(val config: Configuration) {
  val logger = Logger("yfinance")

  val yahooFinanceCsvUrl = config.get[String]("endpoints.yahoofinance")
  val csvDataFolder = config.get[String]("folders.csv")
  
  def getStockCsv(ticker: String, from: String, to: String, interval: String = "1d"): Unit = {
    def parseDate(date: String) = new SimpleDateFormat("yyyy-MM-dd").parse(date)

    val fromTimestamp = parseDate(from).getTime() / 1000
    val toTimestamp = parseDate(to).getTime() / 1000

    // TODO get full dataset if no from/to specified
    val url = new URL(s"$yahooFinanceCsvUrl$ticker?period1=$fromTimestamp&period2=$toTimestamp&interval=$interval")
    val filename = s"$csvDataFolder/$ticker-$from-$to.csv"

    val connection = url.openConnection().asInstanceOf[HttpURLConnection]
    connection.connect()

    // TODO maybe implement a better way of handling exceptions here
    if (connection.getResponseCode() >= 400)
      logger.error(s"Error ${connection.getResponseCode()} for $url")
    else
      url #> new File(filename) !!
  }
}