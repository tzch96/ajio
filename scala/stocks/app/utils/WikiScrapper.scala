package utils

import com.typesafe.scalalogging.Logger
import javax.inject._
import org.jsoup.Jsoup
import play.api.Configuration

import collection.JavaConverters._

// A helper tool to get current SP500 index companies from Wikipedia, so that we can fetch their data at program start
// (we assume that those stocks will be most popular therefore most needed, and fetch them at start to save time later on)
@Singleton
class WikiScrapper @Inject()(val config: Configuration) {
  val logger = Logger("wikiscrapper")
  
  val wikiSP500Url = config.get[String]("endpoints.wikiSP500")

  def getSP500Tickers(): Seq[String] = {
    val doc = Jsoup.connect(wikiSP500Url).get()

    val el = doc.select("table.wikitable td:eq(0) a:eq(0)")

    el.asScala.map(_.text()).toSeq
  }
}