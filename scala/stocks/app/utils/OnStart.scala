package utils

import dao.StocksDao

import com.google.inject.AbstractModule
import com.typesafe.scalalogging.Logger
import javax.inject._
import play.api.Configuration
import play.api.db.Database
import play.api.inject.ApplicationLifecycle

import scala.concurrent.Future
import java.nio.file._

@Singleton
class OnStart @Inject() (val config: Configuration,
                         val lifecycle: ApplicationLifecycle,
                         val database: Database) {
  val logger = Logger("onstart")
  val csvDataFolder = config.get[String]("folders.csv")
  val skipAutoPredictionOnStartup = config.get[Boolean]("app.skipAutoPredictionOnStartup")
  
  val fetcher = new YFinanceFetcher(config)
  val stocksDao = new StocksDao(config, database)

  val SP500Tickers = new WikiScrapper(config).getSP500Tickers
  
  val currentDate = java.time.LocalDate.now()
  val oneYearAgo = currentDate.minusYears(1)

  for (ticker <- SP500Tickers) {
    if (Files.notExists(Paths.get(s"$csvDataFolder/$ticker-${oneYearAgo.toString}-${currentDate.toString}.csv")))
      fetcher.getStockCsv(ticker, oneYearAgo.toString, currentDate.toString)
  }

  for (file <- (new java.io.File(csvDataFolder)).listFiles.filter(_.isFile).filter(_.length > 1024).toList) {
    stocksDao.getCsvStocksToDatabase(file)
  }

  for (ticker <- SP500Tickers) {
    // TODO automatically skip if predictions already in database
    if (!skipAutoPredictionOnStartup)
      stocksDao.getPredictionsToDatabase(ticker, Some(currentDate.toString))
  }
  
  lifecycle.addStopHook { () =>
    Future.successful()
  }
}

class StartModule extends AbstractModule {
  override def configure() = {
    bind(classOf[OnStart]).asEagerSingleton()
  }
}