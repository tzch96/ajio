package controllers

import com.typesafe.scalalogging.Logger
import javax.inject._
import play.api._
import play.api.data._
import play.api.data.Forms._
import play.api.db.Database
import play.api.mvc._

import dao.UserStocksDao

@Singleton
class UserStocksController @Inject()(val controllerComponents: ControllerComponents,
                                     val config: Configuration,
                                    val database: Database) extends BaseController
{
  val logger = Logger("userstockscontroller")

  val userStocksDao = new UserStocksDao(config, database)

  def addStock() = Action { implicit request: Request[AnyContent] =>
    val newTicker = request.body.asFormUrlEncoded.get("new_ticker").head
    val buyDate = request.body.asFormUrlEncoded.get("buy_date").head
    val shares = request.body.asFormUrlEncoded.get("shares").head

    userStocksDao.addStock(newTicker, buyDate, None, shares.toInt)
    
    Redirect("/")
  }
}
