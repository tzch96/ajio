package controllers

import com.typesafe.scalalogging.Logger
import javax.inject._
import play.api._
import play.api.db.Database
import play.api.mvc._

import dao.StocksDao
import dao.UserStocksDao

@Singleton
class HomeController @Inject()(val controllerComponents: ControllerComponents,
                               val config: Configuration,
                               val database: Database) extends BaseController
{
  val logger = Logger("home")

  val stocksDao = new StocksDao(config, database)
  val userStocksDao = new UserStocksDao(config, database)

  def chart(ticker: String, labels: String, data: String) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.chart(ticker, labels, data))
  }

  def index() = Action { implicit request: Request[AnyContent] =>
    val getUserTickers = userStocksDao.getUserTickers()

    val getUserStocks = userStocksDao.getAllUserStocks()
    val valuesFromTo = getUserStocks.map(t => stocksDao.getValuesFromTo(t._1, t._2, "2021-02-05"))

    val params = valuesFromTo.map(v => (v._1, "\"" + v._2.map(_._1).mkString("\", \"") + "\"", v._2.map(_._2).mkString(", ")))

    Ok(views.html.index(params))
  }
}
