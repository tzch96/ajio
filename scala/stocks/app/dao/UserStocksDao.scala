package dao

import anorm._
import com.typesafe.scalalogging.Logger
import javax.inject._
import play.api.Configuration
import play.api.db.Database

import scala.collection.mutable.ListBuffer

@Singleton
class UserStocksDao @Inject() (val config: Configuration,
                               val database: Database) {
  val logger = Logger("userstocksdao")
  val stocksDao = new StocksDao(config, database)

  def insertValuesToDatabase(ticker: String, data: Iterator[(String, String)]): Unit = {
    def toDouble(value: String) = {
      try {
        value.toDouble
      } catch {
        case nfe: NumberFormatException => 0.0
      }
    }

    data.foreach { item =>
      database.withConnection { implicit conn => 
        SQL("INSERT INTO stock_prices(price_id, ticker, price_date, close_price)" +
          " VALUES (NEXTVAL('s_stock_prices'), {ticker}, TO_DATE({price_date}, 'YYYY-MM-DD'), {close_price})" +
          " ON CONFLICT (ticker, price_date) DO NOTHING")
          .on("ticker" -> ticker, "price_date" -> item._1, "close_price" -> toDouble(item._2)).executeInsert()
      } 
    }
  }

  def addStock(ticker: String, buyDate: String, sellDate: Option[String], shares: Int) = {
    database.withConnection { implicit conn =>
      SQL("INSERT INTO user_stocks(user_stock_id, ticker, buy_date, sell_date, current_value, shares)" +
        " VALUES (NEXTVAL('s_user_stocks'), {ticker}, TO_DATE({buy_date}, 'YYYY-MM-DD'), TO_DATE({sell_date}, 'YYYY-MM-DD'), {current_value}, {shares})" +
        " ON CONFLICT (ticker, buy_date) DO NOTHING")
        .on("ticker" -> ticker,
          "buy_date" -> buyDate,
          "sell_date" -> sellDate.getOrElse("2021-03-30"),
          "current_value" -> stocksDao.getCurrentValue(ticker),
          "shares" -> shares).executeInsert()
    }
  }

  def getUserTickers() = {
    database.withConnection { implicit conn =>
      val stmt = conn.createStatement
      val rs = stmt.executeQuery(s"SELECT DISTINCT ticker FROM user_stocks")

      val listBuffer = ListBuffer[String]()

      while (rs.next()) {
        listBuffer.append(rs.getString("ticker"))
      }

      listBuffer.toList
    }
  }

  def getAllUserStocks() = {
    database.withConnection { implicit conn =>
      val stmt = conn.createStatement
      val rs = stmt.executeQuery(s"SELECT ticker, buy_date, shares FROM user_stocks")

      val listBuffer = ListBuffer[(String, String, Int)]()

      while (rs.next()) {
        listBuffer.append((rs.getString("ticker"), rs.getString("buy_date"), rs.getInt("shares")))
      }

      listBuffer.toList
    }
  }
}