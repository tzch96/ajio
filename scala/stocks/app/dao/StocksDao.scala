package dao

import anorm._
import com.typesafe.scalalogging.Logger
import javax.inject._
import org.apache.http._
import org.apache.http.client._
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.util.EntityUtils
import play.api.Configuration
import play.api.db.Database
import play.api.libs.json._

import scala.collection.mutable.ListBuffer
import scala.io.Source
import java.io.File

@Singleton
class StocksDao @Inject() (val config: Configuration,
                           val database: Database) {
  val logger = Logger("stocksdao")

  val nextValuesUrl = config.get[String]("endpoints.nextvalues")

  def insertValuesToDatabase(ticker: String, data: Iterator[(String, String)]): Unit = {
    def toDouble(value: String) = {
      try {
        value.toDouble
      } catch {
        case nfe: NumberFormatException => 0.0
      }
    }

    data.foreach { item =>
      database.withConnection { implicit conn => 
        SQL("INSERT INTO stock_prices(price_id, ticker, price_date, close_price)" +
          " VALUES (NEXTVAL('s_stock_prices'), {ticker}, TO_DATE({price_date}, 'YYYY-MM-DD'), {close_price})" +
          " ON CONFLICT (ticker, price_date) DO NOTHING")
          .on("ticker" -> ticker, "price_date" -> item._1, "close_price" -> toDouble(item._2)).executeInsert()
      }
    }
  }

  def getCsvStocksToDatabase(file: File): Unit =  {
    val ticker = file.getName().substring(0, file.getName().indexOf("-2"))

    val bufferedSource = Source.fromFile(file)

    // get date - close price pairs
    val data = bufferedSource.getLines().drop(1).map { line =>
      val splitted = line.split(",")
      val date = splitted(0)
      val value = if (splitted.size > 4) splitted(4) else "0.0"

      (date, value)
    }

    insertValuesToDatabase(ticker, data)

    logger.debug(s"Got CSV values to database for $ticker")
  }

  def getValuesFromTo(ticker: String, fromDate: String, toDate: String): (String, List[(String, Double)]) = {
    database.withConnection { implicit conn =>
      val stmt = conn.createStatement
      val rs = stmt.executeQuery(s"SELECT price_date, close_price FROM stock_prices WHERE ticker = '$ticker' AND price_date > '$fromDate' AND price_date < '$toDate'" +
        s" ORDER BY price_date ASC")

      val listBuffer = ListBuffer[(String, Double)]()

      while (rs.next()) {
        listBuffer.append((rs.getString("price_date"), rs.getDouble("close_price")))
      }

      (ticker, listBuffer.toList)
    }
  }

  def getCurrentValue(ticker: String): Double = {
    database.withConnection { implicit conn =>
      val stmt = conn.createStatement
      val rs = stmt.executeQuery(s"SELECT close_price FROM stock_prices WHERE ticker = '$ticker' AND price_date <= NOW() AND price_date > NOW() - INTERVAL '3 days' LIMIT 1")

      if (rs.next()) {
        rs.getDouble("close_price")
      } else {
        // TODO more sensible else result
        0.0
      }
    }
  }

  def getCurrentValues(ticker: String, toDate: Option[String]): List[Double] = {
    database.withConnection { implicit conn =>
      val stmt = conn.createStatement
      val rs = {
        if (toDate.isEmpty)
          stmt.executeQuery(s"SELECT close_price FROM stock_prices WHERE ticker = '$ticker' ORDER BY price_date ASC")
        else
          stmt.executeQuery(s"SELECT close_price FROM stock_prices WHERE ticker = '$ticker' AND price_date < '${toDate.get}' ORDER BY price_date ASC")
      }

      val listBuffer = ListBuffer[Double]()

      while (rs.next()) {
        listBuffer.append(rs.getDouble("close_price"))
      }

      listBuffer.toList
    }
  }

  def predictNextValues(currentValues: List[Double], numPredictions: Int): Seq[Double] = {
    val json: JsValue = JsObject(
      Seq(
        "values" -> Json.toJson(currentValues),
        "n_predictions" -> JsNumber(numPredictions)
      )
    )

    val jsonString = json.toString
    val jsonStringEntity = new StringEntity(jsonString)

    val client = new DefaultHttpClient
    val post = new HttpPost(nextValuesUrl)
    
    post.setEntity(jsonStringEntity)

    post.setHeader("Accept", "application/json")
    post.setHeader("Content-type", "application/json")

    val response = client.execute(post)
    if (response.getStatusLine().getStatusCode() != 200) {
      logger.error(s"Response from the server ${response.getStatusLine.getStatusCode()}")
      Nil
    }
    val responseString = EntityUtils.toString(response.getEntity())

    client.close();

    responseString.substring(1, responseString.length() - 1).split(", ").map(_.toDouble).toList
  }

  def getPredictionsToDatabase(ticker: String, fromDate: Option[String], numPredictions: Int = 30): Unit = {
    def parseDate(date: String) = java.time.LocalDate.parse(date)

    val currentValues = getCurrentValues(ticker, fromDate)
    val predictions = predictNextValues(currentValues, numPredictions)

    val dates = (1 to numPredictions).map(i => parseDate(fromDate.get).plusDays(i))

    val data = predictions.map { prediction =>
      val date = dates(predictions.indexOf(prediction))
      
      (date.toString, prediction.toString)
    }.iterator

    insertValuesToDatabase(ticker, data)

    logger.debug(s"Got prediction values to database for $ticker for $numPredictions days from ${fromDate.getOrElse("No date provided")}")
  }
}